public class TestAll { // save as "TestCylinder.java"
        public static void main(String[] args) {

                Shape unknownShape1 = new Shape();
                Shape unknownShape2 = new Shape("purple", false);
                Shape unknownShape3 = new Shape();
                unknownShape3.setColor("Magenta");
                unknownShape3.setFilled(true);
                System.out.println(unknownShape1.toString());
                System.out.println(unknownShape2.toString());
                System.out.println(unknownShape3.toString());

                System.out.println("\n=================================\n");

                Circle C1 = new Circle();
                Circle C2 = new Circle(2.0);
                Circle C3 = new Circle(2.0, "black", false);
                System.out.println(C1.toString());
                System.out.println(C2.toString());
                System.out.println(C3.toString());

                System.out.println("\n=================================\n");

                Rectangle Rec1 = new Rectangle();
                Rectangle Rec2 = new Rectangle(2, 3);
                Rectangle Rec3 = new Rectangle(2, 3, "pink", false);
                System.out.println(Rec1);
                System.out.println(Rec2);
                System.out.println(Rec3);

                System.out.println("\n=================================\n");

                Square Sq1 = new Square();
                Square Sq2 = new Square(5);
                Square Sq3 = new Square(5, "pink", false);
                System.out.println(Sq1);
                System.out.println(Sq2);
                System.out.println(Sq3);

                System.out.println("\n=================================\n");

        }
}
