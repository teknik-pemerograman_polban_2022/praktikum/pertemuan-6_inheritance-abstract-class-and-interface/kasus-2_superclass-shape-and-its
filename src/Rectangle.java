public class Rectangle extends Shape {
    private double width;
    private double length;

    public double getWidth() {
        return this.width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return this.length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public Rectangle() {
        width = 1.0;
        length = 1.0;
    }

    public Rectangle(double with, double length) {
        this.width = with;
        this.length = length;
    }

    public Rectangle(double with, double length, String color, boolean filled) {
        this.width = with;
        this.length = length;
        super.setColor(color);
        super.setFilled(filled);
    }

    public double getArea() {
        return width * length;
    }

    public double getPerimeter() {
        return 2 * (width + length);
    }

    public String toString() {
        return "A Rectangle with width=" + width + " and length=" + length + ", which is a subclass of "
                + super.toString();
    }
}
