public class Square extends Rectangle {
    public Square() {
        super();
    }

    public Square(double side) {
        super(side, side); // Call superclass Rectangle(double, double)
    }

    public Square(double side, String color, boolean filled) {
        this(side);
        super.setColor(color);
        super.setFilled(filled);
    }

    @Override
    public void setLength(double length) {
        super.setLength(length);
        super.setWidth(length);
    }

    @Override
    public void setWidth(double width) {
        super.setLength(width);
        super.setWidth(width);
    }

    public String toString() {
        return "A Square with side=" + super.getWidth() + ", which is a subclass of " + super.toString();
    }
}
